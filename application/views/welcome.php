<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Vitajte!</title>
	
	<?php 
	$this->load->helper('html');
	echo link_tag('assets/css/assets/jquery-ui-1.10.3.custom.css');
	echo link_tag('assets/css/assets/welcome_page.css');	
	?>
	
	<script src="http://localhost/gym/assets/js/jquery-1.9.1.js" type="text/javascript" charset="utf-8"></script>
	<script src="http://localhost/gym/assets/js/jquery-ui-1.10.3.custom.js" type="text/javascript" charset="utf-8"></script>

	<script>
		$(document).ready(function () {
			$( ".jquery_button" ).button();
		});
	</script>
</head>
<body>

<div id="welcome_menu">
	<div id="welcome_menu_login" class="welcome_menu_item">
		<!--button id="welcome_menu_login_button" class="jquery_button" title="Kliknite pre prihlásenie sa do systému">
			Prihlásenie
		</button -->
		<a href="home/login">Prihlásenie</a>
	</div>
	<div id="welcome_menu_about_us" class="welcome_menu_item">
		<button id="welcome_menu_about_us_button" class="jquery_button" title="Pár informácií o nás">
			O nás
		</button>
	</div>
	<div id="welcome_menu_gallery" class="welcome_menu_item">
		<button id="welcome_menu_gallery_button" class="jquery_button" title="Interiér i exteriér">
			Fotogaléria
		</button>
	</div>
	<div id="welcome_menu_price_list" class="welcome_menu_item">
		<button id="welcome_menu_price_list_button" class="jquery_button" title="Aktuálny cenník">
			Cenník
		</button>
	</div>
	<div id="welcome_menu_contact" class="welcome_menu_item">
		<button id="welcome_menu_contact_button" class="jquery_button" title="Neváhajte nás kontaktovať">
			Kontakt
		</button>
	</div>
</div>

<div id="container">
	<!-- h1>Vitajte na stránkach Vašej posilňovne!</h1 -->
</div>

</body>
</html>
