<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
	}

	function index()
	{
		
		$valid_result_nick;
		$valid_result_email;
		$valid_result_phone;
		$valid_result_password;
		
		$this->load->helper(array('form', 'url'));
		
		//$this->load->library('form_validation');
		$this->load->library('form_validation');
		
		log_message('debug', 'setting rules' );
		
		// setting validation for nick
		$this->form_validation->set_rules('nick', 'Nick', 'trim|max_length[32]|xss_clean|callback_nick_check');
		
		// validate
		$valid_result_nick = $this->form_validation->run();

		// unsetting validation for nick
//		$this->form_validation->set_rules('nick', 'Nick', '');
		$this->form_validation->clear_field_data();	

		// setting validation for email
		$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|callback_email_check');
		
		// validate
		$valid_result_email = $this->form_validation->run();
	
		// unsetting validation for email
		//$this->form_validation->set_rules('email', 'Email', '');
		$this->form_validation->clear_field_data();
			
		// setting validation for phone
		$this->form_validation->set_rules('phone', 'Phone', 'trim|min_length[10]|max_length[32]|callback_phone_check');
		
		// validate
		$valid_result_phone = $this->form_validation->run();
		
		// unsetting validation for phone
		//$this->form_validation->set_rules('phone', 'Phone', '');
		$this->form_validation->clear_field_data();
	
		//|matches[passconf]|md5 possible to use regex mehehe
		$this->form_validation->set_rules('password', 'Password', 'trim|required|callback_password_check');
		
		// validate
		$valid_result_password = $this->form_validation->run();
				
		// unsetting validation for phone
		//$this->form_validation->set_rules('password', 'Password', '');
		$this->form_validation->clear_field_data();

		log_message('debug', 'after rules' );
		
		
		log_message('debug','valid_result_nick '.var_export($valid_result_nick, true) );
		log_message('debug','valid_result_email '.var_export($valid_result_email, true) );
		log_message('debug','valid_result_phone '.var_export($valid_result_phone, true) );
		log_message('debug','valid_result_password '.var_export($valid_result_password, true) );
	
		$form_data_array = array(
			'nick' => array( 
				'column_name' => 'nick',
				'is_valid' => $valid_result_nick,
				'column_data' => 'puwel' //( isset( $this->i_nick ) ? this->i_nick : '' )
				) ,
			'email' => array(
				'column_name' => 'email',
				'is_valid' => $valid_result_email,
				'column_data' => 'pavol.dano@gmail.com'//( isset( $this->i_email) ? $this->i_email : '' )
				) ,
			'phone' => array(
				'column_name' => 'phone',
				'is_valid' => $valid_result_phone,
				'column_data' => '+421915507714'//( isset( $this->i_phone ) ? $this->i_phone : '' )
				 )
			);
			
		
		//$this->form_validation->run() == FALSE)
		if ( ($valid_result_nick OR $valid_result_email OR $valid_result_phone) AND $valid_result_password )
		{
			// valid data, view again and attach info error messages
			
			if( $this->is_authentified_any( $form_data_array, 'heslo' ) )
			{
				// login successful
			
				log_message('debug', 'Login successful.');
			
				$this->load->view('home/login_success');
			}else
			{
				log_message('debug', 'Everything set but user has not beed authentified.');
				//log_message('debug', isset( $this->form_validation->password ));
				// password set but user has not beed authentified
				$this->load->view('home/login');
			}
		}/*
		else if( isset( $this->form_validation->password ) )
		{
			log_message('debug', 'IN!');

			if( $this->is_authentified_any( $form_data_array, $this->form_validation->password) )
			{
				// login successful
			
				log_message('debug', 'Login successful.');
			
				$this->load->view('home/login_success');
			}else
			{
				log_message('debug', 'password set but user has not beed authentified.');
				
				// password set but user has not beed authentified
				$this->load->view('home/login');
			}
		}*/
		else
		{
			// any combination of bad input
			
			$this->load->view('home/login');
		}
		
	}
	
	public function nick_check( $nick )
	{
		
		log_message('debug', 'nick_check() for nick ' . $nick );
		

		if( isset( $nick ) AND !is_null( $nick ) AND $nick != '')
		{
			// nick is set
			
			log_message('debug', 'Nick is set as: '. $nick );
//			$this->i_nick = $nick;
			
			// check if equals 'test'	
			if ($nick == 'test')
			{
				log_message('debug', 'Checking comparison to word "test", which is true' );
				
				$this->form_validation->set_message('nick_check', 'Slovo "test" sa nesmie použiť pre %s');
				return FALSE;
			}
			
			// check if exists in database
			if ( $this->user_model->is_nick_persistant( $nick ) == FALSE )
			{
				$this->form_validation->set_message('nick_check', 'Takýto nick v systéme neexistuje');
				
				return FALSE;
			}
			
			return TRUE;
		} else {
			// nick does not have to be used, maybe user used email or phone to log in
			
			log_message('debug', 'Nick is not set');
			
			return TRUE;
		}
	}
	
	public function email_check( $email )
	{
		log_message('debug', ' email_check() for email ' . $email );
		
		if( isset( $email ) AND !is_null( $email ) AND $email != '')
		{
			// email is set
//			$this->i_email=$email;
			
			log_message('debug', 'Email is set as: '. $email );
			
			// check if exists in database
			if ( $this->user_model->is_email_persistant( $email ) == FALSE )
			{

				$this->form_validation->set_message('email_check', 'Takýto email v systéme neexistuje');
				
				return FALSE;
			}
			
			return TRUE;
		} else {
			// email does not have to be used, maybe user used nick or email to log in
			
			log_message('debug', 'Email nubmer is not set');
			
			return TRUE;
		}
	}
	
	public function phone_check( $phone )
	{
		
		log_message('debug', 'phone_check() for phone ' . $phone );
		
		if( isset( $phone ) AND !is_null( $phone ) AND $phone != '')
		{
			// phone is set
//			$this->i_phone = $phone;
			
			log_message('debug', 'Phone is set as: '. $phone );
			
			// check if exists in database
			if ( $this->user_model->is_phone_persistant( $phone ) == FALSE )
			{

				$this->form_validation->set_message('phone_check', 'Takéto telefónne číslo v systéme neexistuje');
				
				return FALSE;
			}
			
			return TRUE;
		} else {
			// phone does not have to be used, maybe user used nick or email to log in
			
			log_message('debug', 'Phone nubmer is not set');
			
			return TRUE;
		}
	}
	
	public function password_check( $pass ){
//				$this->i_password = $pass;
	}


	// $form_data_array is multidimensional array
	public function is_authentified_any( $form_data_array, $password_value )
	{
	//	log_message('debug','here!');
		
		foreach( $form_data_array as $single_row ){
			// if valid input
			if( $single_row['is_valid'] == TRUE)
			{	
				//try to perform authentification
				if ( $this->user_model->is_authentified( $single_row['column_name'], $single_row['column_data'], $password_value ) == TRUE )
				{
					return TRUE;
				}
			}
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/home/login.php */
