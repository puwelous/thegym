<?php
class User_model extends CI_Model {

	public function __construct()
	{
		
		log_message('debug', 'Loading database...');
		
		$this->load->database();
		
		log_message('debug', 'Databse successfully loaded...');
	}
	
	public function is_nick_persistant( $nick )
	{
	
		log_message('debug', 'Looking for user with nick ' . $nick . ' in a table user.');
	
		$result = $this->db->get_where('user', array('nick' => $nick))->row_array();
		
		if ( empty( $result ) ){
			return FALSE;
		}else{
			return TRUE;
		}
	}
	
	public function is_phone_persistant( $phone )
	{
	
		log_message('debug', 'Looking for user with phone ' . $phone . ' in a table user.');
	
		$result = $this->db->get_where('user', array('phone' => $phone))->row_array();
		
		if ( empty( $result ) ){
			return FALSE;
		}else{
			return TRUE;
		}
	}
	
	public function is_email_persistant( $email )
	{
	
		log_message('debug', 'Looking for user with email ' . $email . ' in a table user.');
		
		$result = $this->db->get_where('user', array('email' => $email))->row_array();
		
		if ( empty( $result ) ){
			return FALSE;
		}else{
			return TRUE;
		}
	}
	
	
	public function is_authentified( $column_name, $column_value, $password_value)
	{
		// $column_name = 'email', column_value = 'user@mail.com'
		
		log_message('debug', 'is_authentified called() ');
		
//		try{
			$result = $this->db->get_where(
				'user',
				array(
					$column_name => $column_value,
					'password' => $password_value)
			)->row_array();
//		}catch( Exception e){
//			log_message('debug', $this->db->_error_message() );
//		}
		
		if ( empty( $result ) ){
			return FALSE;
		}else{
			return TRUE;
		}
	}
	
}
/* End of file user_model.php */
/* Location: ./application/models/user_model.php */
